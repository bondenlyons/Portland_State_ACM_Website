# Portland State University ACM Website

Copyright (c) 2018 Bonden Lyons

bonden@pdx.edu

The goal of this project is the overhaul of the website for the PSU student chapter of the Association for Computing Machinery. Our chapter is going through a rebuilding phase and we are in need of a new website that is inviting to those PSU students who may be interested in joining our ranks. The new website should be clean, straightforward, and responsive.

The files contained in this project are HTML, CSS, and Javascript files, no build required, and their use is straightforward.

## License

This work is licensed under the GPL version 3 or later. See the file LICENSE in this distribution for license terms.